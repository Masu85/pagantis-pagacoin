import { NgModule, Optional, SkipSelf } from '@angular/core';
import { UsersService }                 from './services/users.service';
import { CommonModule }                 from '@angular/common';
import { HttpClientModule }             from '@angular/common/http';
import { WalletsService }               from './services/wallets.service';
import { TransferHistoryService }       from './services/transferHistory.service';

@NgModule({
            declarations: [],
            providers:    [
              UsersService,
              WalletsService,
              TransferHistoryService
            ],
            imports:      [
              CommonModule,
              HttpClientModule
            ]
          })
export class CoreModule {

  constructor(@Optional() @SkipSelf() core: CoreModule) {
    if(core) {
      throw new Error('Core module should only be imported in AppModule');
    }
  }
}
