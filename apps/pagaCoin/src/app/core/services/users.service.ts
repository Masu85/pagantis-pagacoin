import { Injectable }                    from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError }        from 'rxjs';
import { UserI }                         from '../interfaces/UserI';
import { catchError }                    from 'rxjs/operators';


@Injectable()
export class UsersService
{
    private usersEndpoint = 'http://localhost:8080/users/';

    constructor(private http: HttpClient) {}

    getAllUsers(): Observable<any>
    {
        return this.http.get<UserI>(this.usersEndpoint + 'all').pipe(catchError(this.handleError))
    }

    getUserById(id: string): Observable<any>
    {
        return this.http.get<UserI>(this.usersEndpoint + id).pipe(catchError(this.handleError))
    }

    private handleError(error: HttpErrorResponse): any
    {
        if (error.error instanceof ErrorEvent)
        {
            console.error('An error occurred:', error.error.message);
        }
        else
        {
          console.error(`Backend returned code ${error.status}, ` +
                        `body was: ${error.error}`);
        }

        return throwError('Something bad happened; please try again later.');
    }
}
