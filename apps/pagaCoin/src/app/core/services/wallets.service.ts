import { Injectable }                    from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError }        from 'rxjs';
import { catchError }                    from 'rxjs/operators';
import { WalletsI }                      from '../interfaces/WalletsI';


@Injectable()
export class WalletsService
{
  private walletsEndpoint = 'http://localhost:8080/wallets/';

  constructor(private http: HttpClient) {}

  getAllWallets(): Observable<any>
  {
    return this.http.get<WalletsI>(this.walletsEndpoint + 'all').pipe(catchError(this.handleError))
  }

  getUserWallets(idUser: string): Observable<any>
  {
    return this.http.get<WalletsI>(this.walletsEndpoint + 'user/' + idUser).pipe(catchError(this.handleError))
  }

  doTransferBetweenWallets(idOriginWallet: string, idDestinationWallet: string, amount: number)
  {
      return this.http.get<any>(this.walletsEndpoint + 'send?' + 'originWallet=' + idOriginWallet
                                                                  + '&destinationWallet=' + idDestinationWallet
                                                                  + '&amount=' + amount);
  }

  private handleError(error: HttpErrorResponse): any
  {
    if (error.error instanceof ErrorEvent)
    {
      console.error('An error occurred:', error.error.message);
    }
    else
    {
      console.error(`Backend returned code ${error.status}, ` +
                      `body was: ${error.error}`);
    }

    return throwError('Something bad happened; please try again later.');
  }
}
