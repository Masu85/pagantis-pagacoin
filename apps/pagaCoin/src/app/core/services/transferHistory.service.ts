import { Injectable }                    from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError }        from 'rxjs';
import { catchError }                    from 'rxjs/operators';
import { TransferI }                     from '../interfaces/TransferI';

@Injectable()
export class TransferHistoryService
{
  private usersEndpoint = 'http://localhost:8080/transactions/';

  constructor(private http: HttpClient) {}

  getAllTransfers(): Observable<any>
  {
    return this.http.get<TransferI>(this.usersEndpoint + 'all').pipe(catchError(this.handleError));
  }

  getTransfersByWalletId(walletId: string): Observable<any>
  {
    return this.http.get<TransferI>(this.usersEndpoint + 'wallet/' + walletId).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse): any
  {
    if (error.error instanceof ErrorEvent)
    {
      console.error('An error occurred:', error.error.message);

    }
    else
    {
      console.error(`Backend returned code ${error.status}, ` +
                    `body was: ${error.error}`);
    }

    return throwError('Something bad happened; please try again later.');
  }
}
