import { WalletsI } from './WalletsI';

export interface UserI
{
  id:   string;
  name:     string;
  surname:  string;
  email:    string;
  wallets:  WalletsI[];
}
