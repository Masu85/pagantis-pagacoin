export interface TransferI
{
  id:                 string;
  originWallet:       string;
  destinationWallet:  string;
  amount:             string;
  transactionDate:    string;
}
