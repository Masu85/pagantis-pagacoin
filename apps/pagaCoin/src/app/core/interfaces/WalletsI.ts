import { TransferI } from './TransferI';

export interface WalletsI
{
  id:           string;
  ownerUserId:  string;
  balance:      string;
  transfers:    TransferI[];
}
