import { Component, OnInit }      from '@angular/core';
import { WalletsI }               from '../../core/interfaces/WalletsI';
import { WalletsService }         from '../../core/services/wallets.service';
import { UsersService }           from '../../core/services/users.service';
import { TransferHistoryService } from '../../core/services/transferHistory.service';

@Component({
              selector: 'admin-wallets',
              templateUrl: './wallets.page.html',
              styleUrls: ['./wallets.page.css'],
           })
export class WalletsPage implements OnInit
{
  public showHide: number = -1;
  public walletSelected: string = "";
  public showTransferComponent: boolean = false;
  public wallets: WalletsI[] = [];
  public usersWallet: Map<any, any> = new Map();

  constructor(private walletsService: WalletsService, private userService: UsersService, private transferHistoryService: TransferHistoryService){}

  public ngOnInit(): void
  {
     this.loadWallets();
  }

  showTransfer(walletId: string)
  {
      this.showTransferComponent = !this.showTransferComponent;
      this.walletSelected = walletId;
  }

  hideTransfers()
  {
      this.showTransferComponent = false;
      this.loadWallets();
  }

  loadWallets()
  {
    this.walletsService.getAllWallets().subscribe((resp: any) =>
    {
        this.wallets = resp;
        this.wallets.forEach((wallet, index) =>
        {
           if(this.usersWallet.get(wallet.ownerUserId) == undefined)
           {
              this.userService.getUserById(wallet.ownerUserId).subscribe(user =>
              {
                  this.usersWallet.set(wallet.ownerUserId, user);
              })
           }

           this.transferHistoryService.getTransfersByWalletId(wallet.id).subscribe(transfers =>
           {
              this.wallets[index].transfers = transfers;
           })
        });
    })
  }
}
