import { NgModule }             from '@angular/core';
import { CommonModule }         from '@angular/common';
import { WalletsRoutingModule } from './wallets-routing.module';
import { CoreModule }           from '../../core/core.module';
import { WalletsPage }          from './wallets.page';
import { ComponentsModule }     from '../../components/components.module';


@NgModule({
            imports:      [
              CommonModule,
              WalletsRoutingModule,
              CoreModule,
              ComponentsModule,
            ],
            declarations: [WalletsPage],
            exports: [WalletsPage]
          })
export class WalletsPageModule
{
}
