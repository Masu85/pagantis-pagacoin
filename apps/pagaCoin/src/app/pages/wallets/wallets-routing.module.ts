import { WalletsPage }          from './wallets.page';
import { RouterModule, Routes } from '@angular/router';
import { NgModule }             from '@angular/core';


const routes: Routes = [
  {
    path: '',
    component: WalletsPage
  }
];

@NgModule({
            imports: [RouterModule.forChild(routes)],
            exports: [RouterModule],
          })
export class WalletsRoutingModule{}
