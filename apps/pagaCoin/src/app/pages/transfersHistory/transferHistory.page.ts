import { Component, OnInit }      from '@angular/core';
import { TransferHistoryService } from '../../core/services/transferHistory.service';
import { TransferI }              from '../../core/interfaces/TransferI';

@Component({
             selector: 'admin-transfer-history',
             templateUrl: './transferHistory.page.html',
             styleUrls: ['./transferHistory.page.css'],
           })
export class TransferHistoryPage implements OnInit
{
    public transfer: TransferI[] = [];

    constructor(private transferHistoryService: TransferHistoryService){};

    ngOnInit(): void
    {
      this.loadTransfers();
    }

    loadTransfers()
    {
        this.transferHistoryService.getAllTransfers().subscribe((resp: any) =>
        {
            this.transfer = resp;
        })
    }

}
