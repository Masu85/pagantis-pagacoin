import { RouterModule, Routes } from '@angular/router';
import { NgModule }             from '@angular/core';
import { TransferHistoryPage }  from './transferHistory.page';


const routes: Routes = [
  {
    path: '',
    component: TransferHistoryPage
  }
];

@NgModule({
            imports: [RouterModule.forChild(routes)],
            exports: [RouterModule],
          })
export class TransferHistoryRoutingModule{}
