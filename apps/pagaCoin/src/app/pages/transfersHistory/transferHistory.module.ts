import { CommonModule }                 from '@angular/common';
import { NgModule }                     from '@angular/core';
import { CoreModule }                   from '../../core/core.module';
import { ComponentsModule }             from '../../components/components.module';
import { TransferHistoryRoutingModule } from './transferHistory-routing.module';
import { TransferHistoryPage }          from './transferHistory.page';

@NgModule({
            imports:      [
              CommonModule,
              TransferHistoryRoutingModule,
              CoreModule,
              ComponentsModule
            ],
            declarations: [TransferHistoryPage],
            exports: [TransferHistoryPage]
          })
export class TransferHistoryPageModule
{
}
