import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainContainerPage }    from './mainContainer.page';
import { notFound404 }          from '../components/404/notFound404.component';


const routes: Routes = [
  {
    path: '',
    component: MainContainerPage,
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: '',
    component: MainContainerPage,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
      },
      {
        path: 'users',
        loadChildren: () => import('./users/users.module').then( m => m.UsersPageModule)
      },
      {
        path: 'wallets',
        loadChildren: () => import('./wallets/wallets.module').then( m => m.WalletsPageModule)
      },
      {
        path: 'transfer-history',
        loadChildren: () => import('./transfersHistory/transferHistory.module').then( m => m.TransferHistoryPageModule)
      },
    ]

  },
  {
    path: '**',
    component: notFound404
  }
];

@NgModule({
            imports: [RouterModule.forChild(routes)],
            exports: [RouterModule]
          })
export class PagesRoutingModule {
}
