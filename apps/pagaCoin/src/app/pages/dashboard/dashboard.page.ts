import { Component, OnInit } from '@angular/core';
import { UsersService }      from '../../core/services/users.service';
import { UserI }             from '../../core/interfaces/UserI';
import { WalletsService }    from '../../core/services/wallets.service';
import { WalletsI }          from '../../core/interfaces/WalletsI';

@Component({
             selector: 'admin-dashboard',
             templateUrl: './dashboard.page.html',
             styleUrls: ['./dashboard.page.css'],
           })
export class DashboardPage implements OnInit
{
    public appPages =
    [
      {
        title: 'Dashboard',
        url: '/dashboard/',
        icon: 'dashboard'
      },
      {
        title: 'Users',
        url: '/users',
        icon: 'supervisor_account'
      },
      {
        title: 'Wallets',
        url: '/wallets',
        icon: 'credit_card'
      },
    ];

    public users:   UserI[] = [];
    public wallets: WalletsI[] = [];


    constructor(private usersService: UsersService, private walletsService: WalletsService){}

    ngOnInit(): void
    {
       this.usersService.getAllUsers().subscribe((resp: any) =>
       {
          this.users = resp;
       });

      this.walletsService.getAllWallets().subscribe((resp: any) =>
                                                {
                                                  this.wallets = resp;
                                                });
    }
}
