import { DashboardPage } from './dashboard.page';
import { CommonModule }  from '@angular/common';
import { NgModule }      from '@angular/core';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { CoreModule }             from '../../core/core.module';

@NgModule({
            imports:
            [
              CommonModule,
              DashboardRoutingModule,
              CoreModule
            ],
            declarations: [DashboardPage],
          })
export class DashboardPageModule
{

}
