import { DashboardPage }        from './dashboard.page';
import { RouterModule, Routes } from '@angular/router';
import { NgModule }             from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: DashboardPage
  }
];

@NgModule({
            imports: [RouterModule.forChild(routes)],
            exports: [RouterModule],
          })
export class DashboardRoutingModule{}
