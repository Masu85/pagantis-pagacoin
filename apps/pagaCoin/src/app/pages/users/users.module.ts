import { UsersPage }          from './users.page';
import { CommonModule }       from '@angular/common';
import { NgModule }           from '@angular/core';
import { UsersRoutingModule } from './users-routing.module';
import { CoreModule }         from '../../core/core.module';
import { ComponentsModule }   from '../../components/components.module';

@NgModule({
            imports:      [
              CommonModule,
              UsersRoutingModule,
              CoreModule,
              ComponentsModule
            ],
            declarations: [UsersPage],
            exports: [UsersPage]
          })
export class UsersPageModule
{
}
