import { Component, OnInit } from '@angular/core';
import { UsersService }      from '../../core/services/users.service';
import { WalletsService }    from '../../core/services/wallets.service';
import { UserI }             from '../../core/interfaces/UserI';
import { WalletsI }          from '../../core/interfaces/WalletsI';

@Component({
             selector: 'admin-users',
             templateUrl: './users.page.html',
             styleUrls: ['./users.page.css'],
           })
export class UsersPage implements OnInit
{
    public showHide: number = -1;
    public users:   UserI[] = [];
    public wallets: WalletsI[] = [];
    public walletSelected: string = "";
    public showTransferComponent: boolean = false;

    constructor(private usersService: UsersService, private walletsService: WalletsService){}

    ngOnInit(): void
    {
      this.loadUsers();
    }

    private getWalletsByUser(idUser: string)
    {
      return this.walletsService.getUserWallets(idUser);
    }

    showTransfer(walletId: string)
    {
      this.showTransferComponent = !this.showTransferComponent;
      this.walletSelected = walletId;
    }

    hideTransfers()
    {
      this.showTransferComponent = false;
      this.loadUsers();
    }

    loadUsers()
    {
        this.wallets = [];

        this.usersService.getAllUsers().subscribe((resp: any) =>
        {
            this.users = resp;
            this.users.forEach((user, index) =>
            {
                this.getWalletsByUser(user.id).subscribe(wallets =>
                {
                  this.users[index].wallets = wallets;
                  this.wallets = this.wallets.concat(wallets);
                })
            })
        });
    }
}
