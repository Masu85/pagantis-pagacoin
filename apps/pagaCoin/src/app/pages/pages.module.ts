import { PagesRoutingModule } from './pages-routing.module';
import { CommonModule }       from '@angular/common';
import { NgModule }           from '@angular/core';
import { MainContainerPage }  from './mainContainer.page';

@NgModule({
            declarations: [
              MainContainerPage
            ],
            imports:
            [
              CommonModule,
              PagesRoutingModule
            ],
            exports:
            [
              MainContainerPage
            ]
          })
export class PagesModule {
}
