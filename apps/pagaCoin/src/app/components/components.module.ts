import { NgModule }           from '@angular/core';
import { AdminSideBar }       from './sideBar/sidebar.component';
import { RouterModule }       from '@angular/router';
import { CommonModule }       from '@angular/common';
import { TransfersComponent } from './transfers/transfers.component';
import { FormsModule }        from '@angular/forms';


@NgModule({
            imports:
            [
              RouterModule,
              CommonModule,
              FormsModule
            ],
            exports:
            [
              AdminSideBar,
              TransfersComponent
            ],
            declarations:
            [
              AdminSideBar,
              TransfersComponent
            ]
          })
export class ComponentsModule
{}
