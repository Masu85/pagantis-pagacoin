import { Component, OnInit }     from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { DomSanitizer }          from '@angular/platform-browser';

@Component({
             selector: 'admin-sidebar',
             templateUrl: './sidebar.component.html',
             styleUrls: ['./sidebar.component.css'],
           })
export class AdminSideBar implements OnInit
{
    constructor(private router: Router, private sanitizer: DomSanitizer){}

    public hostUrl = '/';
    public logoImage = "https://crypto.jobs/storage/company-logos/1ggFNVqTluzuJA0bMD5beVXYn4v1y9mYa3rHLZOM.jpeg";

    public selectedIndex = 0;
    public appPages = [
      {
        title: 'Dashboard',
        url: '/dashboard',
        icon: 'dashboard'
      },
      {
        title: 'Users',
        url: '/users',
        icon: 'supervisor_account'
      },
      {
        title: 'Wallets',
        url: '/wallets',
        icon: 'credit_card'
      },
      {
        title: 'TransferHistory',
        url: '/transfer-history',
        icon: 'payments'
      },
    ];

  public ngOnInit(): void
  {
    this.router.events.subscribe((event: any) =>
    {
      if (event instanceof NavigationEnd)
      {
        this.appPages.forEach((value, index) =>
        {
            if(value.url == event.urlAfterRedirects)
            {
                this.selectedIndex = index;
            }
        })
      }
    });
  }
}
