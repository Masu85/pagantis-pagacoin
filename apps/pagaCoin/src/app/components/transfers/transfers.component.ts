import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { WalletsService }                                 from '../../core/services/wallets.service';
import { WalletsI }                                       from '../../core/interfaces/WalletsI';

@Component({
              selector:    'admin-transfers',
              templateUrl: './transfers.component.html',
              styleUrls:   ['./transfers.component.css'],
           })
export class TransfersComponent implements OnInit
{
  @Input() walletSelected: string = '';
  @Input() wallets: WalletsI[] = [];

  @Output() closeEvent = new EventEmitter()

  public error: boolean = false;
  public errorMessage: string = '';
  public amount: number = 0;
  public walletDestination: string = '';
  public walletsToTransfer:  WalletsI[] = [];

  constructor(private walletService: WalletsService){}

  public ngOnInit(): void
  {
      this.wallets.forEach((value) =>
      {
          if(value.id != this.walletSelected)
          {
              this.walletsToTransfer.push(value);
          }
      })
  }

  closeTransfers()
  {
    this.closeEvent.emit();
  }

  doTransfer()
  {
      this.error = false;

      if(this.walletDestination == '')
      {
          this.error = true;
          this.errorMessage = 'Please, select a wallet destination';
          return;
      }
      if(this.amount <= 0)
      {
          this.error = true;
          this.errorMessage = 'We can not transfer 0 or less PagaCoins.';
          return;
      }

      this.walletService.doTransferBetweenWallets(this.walletSelected, this.walletDestination, this.amount).subscribe(value =>
      {
          console.log(value);
          this.closeTransfers();
      },
      error =>
      {
          console.log(error);
          this.error = true;
          if(error.error.message)
          {
            this.errorMessage = error.error.message;
          }
          else
          {
            this.errorMessage = error.error;
          }
      });
  }
}
