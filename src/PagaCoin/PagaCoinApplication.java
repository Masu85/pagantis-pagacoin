package PagaCoin;

import PagaCoin.admin.main.com.pagantis.admin.user.domain.User;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserId;
import PagaCoin.admin.main.com.pagantis.admin.user.infrastructure.repositories.UserRepositoryH2;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.Wallet;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.vo.BalanceVO;
import PagaCoin.admin.main.com.pagantis.admin.wallet.infrastructure.repositories.WalletRepositoryH2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.math.BigDecimal;
import java.util.Optional;

@SpringBootApplication
@Configuration
@EnableSwagger2
@ComponentScan(basePackages = {"PagaCoin.admin", "PagaCoin.shared"})
public class PagaCoinApplication {

	public static void main(String[] args)
	{
		SpringApplication.run(PagaCoinApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer()
	{
		return new WebMvcConfigurer()
		{
			@Override
			public void addCorsMappings(CorsRegistry registry)
			{
				registry.addMapping("/**").allowedOrigins("*");
			}
		};
	}

	@Bean
	CommandLineRunner init(UserRepositoryH2 userRepositoryH2, WalletRepositoryH2 walletRepositoryH2)
	{
		return args ->
		{
			Optional<UserId> userId1 = userRepositoryH2.createUser(new User(null, "Marc", "Masoliver", "masoliver.vial@gmail.com", null));
			Optional<UserId> userId2 = userRepositoryH2.createUser(new User(null, "Xavi", "Lopez", "xavi.lopez@gmail.com", null));
			Optional<UserId> userId3 = userRepositoryH2.createUser(new User(null, "Marta", "García", "marga.garcia@gmail.com 2", null));

			walletRepositoryH2.createWallet(new Wallet(null, new UserId(userId1.get().value()), new BalanceVO(new BigDecimal("1450.37"))));
			walletRepositoryH2.createWallet(new Wallet(null, new UserId(userId1.get().value()), new BalanceVO(new BigDecimal("200"))));
			walletRepositoryH2.createWallet(new Wallet(null, new UserId(userId1.get().value()), new BalanceVO(new BigDecimal("50.3"))));
			walletRepositoryH2.createWallet(new Wallet(null, new UserId(userId2.get().value()), new BalanceVO(new BigDecimal("70.20"))));
			walletRepositoryH2.createWallet(new Wallet(null, new UserId(userId3.get().value()), new BalanceVO(new BigDecimal("791.37"))));
		};
	}
}
