package PagaCoin.shared.main.com.pagantis.shared.domain;

import java.io.Serializable;
import java.util.UUID;

public abstract class Identifier implements Serializable
{
    final protected String value;

    protected Identifier()
    {
        this.value = null;
    }

    public Identifier(String value)
    {
        assureValidUuid(value);
        this.value = value;
    }

    public String value()
    {
        return this.value;
    }

    private void assureValidUuid(String value) throws IllegalArgumentException
    {
        UUID.fromString(value);
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null || getClass() != o.getClass())  return false;

        Identifier that = (Identifier) o;
        return this.value.equals(that.value);
    }
}
