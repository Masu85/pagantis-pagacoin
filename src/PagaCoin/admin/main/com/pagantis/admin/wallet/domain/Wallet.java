package PagaCoin.admin.main.com.pagantis.admin.wallet.domain;

import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserId;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.vo.BalanceVO;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public final class Wallet
{
    private final WalletId  id;
    private final UserId    ownerUserId;
    private final BalanceVO balance;
}
