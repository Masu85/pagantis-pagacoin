package PagaCoin.admin.main.com.pagantis.admin.wallet.domain;

import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserId;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.vo.BalanceVO;

import java.util.List;
import java.util.Optional;

public interface WalletRepository
{
    List<Wallet>            searchAll();
    List<Wallet>            searchWalletsByUserId(UserId userId);
    Optional<Wallet>        searchWallet(WalletId walletId);
    Optional<WalletId>      createWallet(Wallet wallet);
    Optional<BalanceVO>     updateWalletBalance(WalletId walletId, BalanceVO newBalance);
}
