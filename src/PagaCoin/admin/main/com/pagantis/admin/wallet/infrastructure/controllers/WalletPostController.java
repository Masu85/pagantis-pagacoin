package PagaCoin.admin.main.com.pagantis.admin.wallet.infrastructure.controllers;

import PagaCoin.admin.main.com.pagantis.admin.user.application.SearchUserByID;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserId;
import PagaCoin.admin.main.com.pagantis.admin.wallet.application.CreateWallet;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.Wallet;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletId;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.vo.BalanceVO;
import PagaCoin.admin.main.com.pagantis.admin.wallet.infrastructure.controllers.DTOs.WalletPostDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class WalletPostController
{
    private final CreateWallet createWallet;
    private final SearchUserByID searchUserByID;

    @PostMapping("/wallets")
    public ResponseEntity createNewWallet(@RequestBody WalletPostDTO walletPostDTO)
    {
        //Validate User
        if(!this.searchUserByID.searchById(new UserId(walletPostDTO.getOwnerUserId())).isPresent())
        {
            return ResponseEntity.badRequest().body("Owner user not found error") ;
        }

        //Validate Balance

        Optional<WalletId> walletId = this.createWallet.create(new Wallet(null,
                                                                          new UserId(walletPostDTO.getOwnerUserId()),
                                                                          new BalanceVO(walletPostDTO.getBalance())));

        return walletId.isPresent() ? ResponseEntity.ok().body(walletId.get().value()) : ResponseEntity.badRequest().body("Wallet creation error") ;
    }
}
