package PagaCoin.admin.main.com.pagantis.admin.wallet.infrastructure.controllers;

import PagaCoin.admin.main.com.pagantis.admin.wallet.application.SearchWallet;
import PagaCoin.admin.main.com.pagantis.admin.wallet.application.UpdateWalletBalance;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletId;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.vo.BalanceVO;
import PagaCoin.admin.main.com.pagantis.admin.wallet.infrastructure.controllers.DTOs.WalletPutDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class WalletPutController
{
    private final SearchWallet searchWallet;
    private final UpdateWalletBalance updateWalletBalance;

    @PutMapping("/wallets")
    public ResponseEntity updateWalletBalance(@RequestBody WalletPutDTO walletPutDTO)
    {
        //Validate wallet
        if(!this.searchWallet.serachById(new WalletId(walletPutDTO.getId())).isPresent())
        {
            return ResponseEntity.badRequest().body("Wallet not found error") ;
        }

        Optional<BalanceVO> balanceVO = this.updateWalletBalance.updateBalance(new WalletId(walletPutDTO.getId()),
                                                                               new BalanceVO(walletPutDTO.getBalance()));

        return balanceVO.isPresent() ? ResponseEntity.ok().body(balanceVO.get().getValue()) : ResponseEntity.badRequest().body("Wallet update error") ;
    }
}
