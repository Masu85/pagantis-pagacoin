package PagaCoin.admin.main.com.pagantis.admin.wallet.application;

import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserId;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.Wallet;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SearchWalletsByUserId
{
    private final WalletRepository walletRepository;

    public List<Wallet> serachByUserId(UserId userId)
    {
        return this.walletRepository.searchWalletsByUserId(userId);
    }
}
