package PagaCoin.admin.main.com.pagantis.admin.wallet.application;

import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.Wallet;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public final class SearchAllWallets
{
    private final WalletRepository walletRepository;

    public List<Wallet> search()
    {
        return this.walletRepository.searchAll();
    }
}
