package PagaCoin.admin.main.com.pagantis.admin.wallet.infrastructure.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface WalletRepositoryH2Spring extends JpaRepository<WalletDAO, UUID>
{
    List<WalletDAO> findAllByOwnerUserId (UUID ownerUserId);
}
