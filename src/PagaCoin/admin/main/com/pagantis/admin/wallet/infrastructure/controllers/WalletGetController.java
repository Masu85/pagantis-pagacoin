package PagaCoin.admin.main.com.pagantis.admin.wallet.infrastructure.controllers;

import PagaCoin.admin.main.com.pagantis.admin.transaction.application.CreateTransaction;
import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.Transaction;
import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.vo.AmountVO;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserId;
import PagaCoin.admin.main.com.pagantis.admin.wallet.application.SearchAllWallets;
import PagaCoin.admin.main.com.pagantis.admin.wallet.application.SearchWallet;
import PagaCoin.admin.main.com.pagantis.admin.wallet.application.SearchWalletsByUserId;
import PagaCoin.admin.main.com.pagantis.admin.wallet.application.UpdateWalletBalance;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.Wallet;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletId;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.vo.BalanceVO;
import PagaCoin.admin.main.com.pagantis.admin.wallet.infrastructure.controllers.DTOs.WalletGetDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public final class WalletGetController
{
    private final SearchAllWallets      searchAllWallets;
    private final SearchWallet          searchWallet;
    private final SearchWalletsByUserId searchWalletsByUserId;
    private final UpdateWalletBalance   updateWalletBalance;
    private final CreateTransaction     createTransaction;

    @GetMapping("/wallets/all")
    public ResponseEntity getAllWallets()
    {
        return ResponseEntity.ok().body(searchAllWallets.search()
                                                        .stream()
                                                        .map(wallet ->
                                                             new WalletGetDTO(wallet.getId().value(),
                                                                              wallet.getOwnerUserId().value(),
                                                                              wallet.getBalance().getValue().toString())
                                                        ));
    }

    @GetMapping("/wallets/user/{id}")
    public ResponseEntity searchWalletsByUser(@PathVariable UUID id)
    {
        return ResponseEntity.ok(searchWalletsByUserId.serachByUserId(new UserId(id.toString()))
                                                      .stream().map(wallet -> new WalletGetDTO(wallet.getId().value(),
                                                                                               wallet.getOwnerUserId().value(),
                                                                                               wallet.getBalance().getValue().toString())));
    }

    @GetMapping("/wallets/send")
    public ResponseEntity sendAmountBetweenWallets(@RequestParam(required = true, name = "originWallet") UUID originId,
                                                  @RequestParam(required = true, name = "destinationWallet") UUID destinationId,
                                                  @RequestParam(required = true, name = "amount") BigDecimal amount)
    {
        //Get Wallets
        Optional<Wallet> originWallet = this.searchWallet.serachById(new WalletId(originId.toString()));
        Optional<Wallet> destinationWallet = this.searchWallet.serachById(new WalletId(destinationId.toString()));

        if(!originWallet.isPresent())      return ResponseEntity.badRequest().body("Origin wallet not found error");
        if(!destinationWallet.isPresent()) return ResponseEntity.badRequest().body("Destination wallet not found error");


        //Calculate new balances
        BalanceVO newOriginBalance;
        BalanceVO newDestinationBalance;

        try
        {
             newOriginBalance      = new BalanceVO(originWallet.get().getBalance().getValue().subtract(amount));
             newDestinationBalance = new BalanceVO(destinationWallet.get().getBalance().getValue().add(amount));
        }
        catch(RuntimeException ex)
        {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }

        //Update Wallets
        this.updateWalletBalance.updateBalance(originWallet.get().getId(), newOriginBalance);
        this.updateWalletBalance.updateBalance(destinationWallet.get().getId(), newDestinationBalance);


        //Register Transaction
        this.createTransaction.create(new Transaction(null,
                                                      originWallet.get().getId(),
                                                      destinationWallet.get().getId(),
                                                      new AmountVO(amount),
                                                      null));

        return ResponseEntity.ok().body(new HashMap<String, BigDecimal>() {{
                                                                              put("NewOriginBalance", newOriginBalance.getValue());
                                                                              put("NewDestinationBalance", newDestinationBalance.getValue());
                                                                          }});
    }
}
