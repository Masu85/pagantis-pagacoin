package PagaCoin.admin.main.com.pagantis.admin.wallet.domain.vo;

import lombok.Getter;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Getter
public class BalanceVO
{
    private BigDecimal value;

    public BalanceVO(BigDecimal balance)
    {
        assureValid(balance);
        this.value = balance.setScale(2, RoundingMode.DOWN);
    }

    public BalanceVO(String stringbalance)
    {
        try
        {
            BigDecimal balance = new BigDecimal(stringbalance);

            assureValid(balance);
            this.value = balance.setScale(2, RoundingMode.DOWN);
        }
        catch(NumberFormatException e)
        {
            throw new RuntimeException("Balance number or format is wrong");
        }
    }

    private void assureValid(BigDecimal balance)
    {
        if(balance == null)
        {
            throw new RuntimeException("Balance cannot be null");
        }

        if(balance.doubleValue() < 0)
        {
            throw new RuntimeException("Final Balance cannot be negative");
        }
    }
}
