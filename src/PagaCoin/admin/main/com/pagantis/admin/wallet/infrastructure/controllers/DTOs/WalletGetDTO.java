package PagaCoin.admin.main.com.pagantis.admin.wallet.infrastructure.controllers.DTOs;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class WalletGetDTO
{
        private final String id;
        private final String ownerUserId;
        private final String balance;
}
