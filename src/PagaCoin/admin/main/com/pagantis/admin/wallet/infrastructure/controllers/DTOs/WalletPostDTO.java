package PagaCoin.admin.main.com.pagantis.admin.wallet.infrastructure.controllers.DTOs;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class WalletPostDTO
{
    private final String     ownerUserId;
    private final String     balance;
}
