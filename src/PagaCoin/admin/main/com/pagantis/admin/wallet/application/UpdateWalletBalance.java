package PagaCoin.admin.main.com.pagantis.admin.wallet.application;

import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletId;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletRepository;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.vo.BalanceVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UpdateWalletBalance
{
    private final WalletRepository walletRepository;

    @Transactional
    public Optional<BalanceVO> updateBalance(WalletId walletId, BalanceVO balance)
    {
        return walletRepository.updateWalletBalance(walletId, balance);
    }
}
