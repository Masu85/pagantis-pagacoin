package PagaCoin.admin.main.com.pagantis.admin.wallet.application;

import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.Wallet;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletId;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CreateWallet
{
    private final WalletRepository walletRepository;

    public Optional<WalletId> create(Wallet wallet)
    {
        return this.walletRepository.createWallet(wallet);
    }
}
