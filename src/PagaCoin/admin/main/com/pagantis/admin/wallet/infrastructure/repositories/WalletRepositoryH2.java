package PagaCoin.admin.main.com.pagantis.admin.wallet.infrastructure.repositories;

import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserId;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.Wallet;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletId;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletRepository;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.vo.BalanceVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class WalletRepositoryH2 implements WalletRepository
{
    private final WalletRepositoryH2Spring walletRepositoryH2Spring;

    @Override
    public List<Wallet> searchAll()
    {
        return walletRepositoryH2Spring.findAll()
                                       .stream()
                                       .map(walletDAO -> new Wallet(new WalletId(walletDAO.getWalletId().toString()),
                                                                    new UserId(walletDAO.getOwnerUserId().toString()),
                                                                    new BalanceVO(walletDAO.getBalance())))
                                       .collect(Collectors.toList());
    }

    @Override
    public List<Wallet> searchWalletsByUserId(UserId userId)
    {
        return walletRepositoryH2Spring.findAllByOwnerUserId(UUID.fromString(userId.value()))
                                       .stream()
                                       .map(walletDAO -> new Wallet(new WalletId(walletDAO.getWalletId().toString()),
                                                                    new UserId(walletDAO.getOwnerUserId().toString()),
                                                                    new BalanceVO(walletDAO.getBalance())))
                                       .collect(Collectors.toList());
    }

    @Override
    public Optional<Wallet> searchWallet(WalletId walletId)
    {
        return walletRepositoryH2Spring.findById(UUID.fromString(walletId.value()))
                                        .map(walletDAO -> new Wallet(new WalletId(walletDAO.getWalletId().toString()),
                                                                     new UserId(walletDAO.getOwnerUserId().toString()),
                                                                     new BalanceVO(walletDAO.getBalance())));
    }

    @Override
    public Optional<WalletId> createWallet(Wallet wallet)
    {
        return Optional.of(new WalletId(walletRepositoryH2Spring.save(new WalletDAO(null,
                                                                      UUID.fromString(wallet.getOwnerUserId().value()),
                                                                      wallet.getBalance().getValue()))
                                                                .getWalletId().toString()));
    }

    @Override
    public Optional<BalanceVO> updateWalletBalance(WalletId walletId, BalanceVO newBalance)
    {
        Optional<WalletDAO> optionalWalletDAO = walletRepositoryH2Spring.findById(UUID.fromString(walletId.value()));

        if(!optionalWalletDAO.isPresent()) return Optional.empty();


        WalletDAO walletDAO = optionalWalletDAO.get();
        return Optional.of(new BalanceVO(walletRepositoryH2Spring.save(new WalletDAO(walletDAO.getWalletId(),
                                                                                     walletDAO.getOwnerUserId(),
                                                                                     newBalance.getValue()))
                                                                 .getBalance()));
    }
}
