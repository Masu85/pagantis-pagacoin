package PagaCoin.admin.main.com.pagantis.admin.wallet.infrastructure.repositories;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Entity(name = "Wallets")
@RequiredArgsConstructor
@AllArgsConstructor
public class WalletDAO
{
    @Id
    @Column(name = "wallet_id")
    @GeneratedValue(generator = "uuid3")
    @GenericGenerator(name = "uuid3", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID       walletId;

    @Column(name = "owner_user_id")
    private UUID       ownerUserId;

    private BigDecimal balance;
}
