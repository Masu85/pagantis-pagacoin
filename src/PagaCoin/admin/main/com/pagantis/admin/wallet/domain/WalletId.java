package PagaCoin.admin.main.com.pagantis.admin.wallet.domain;

import PagaCoin.shared.main.com.pagantis.shared.domain.Identifier;

public final class WalletId extends Identifier
{
    public WalletId(String value)
    {
        super(value);
    }
}
