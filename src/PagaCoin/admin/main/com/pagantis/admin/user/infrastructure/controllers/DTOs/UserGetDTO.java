package PagaCoin.admin.main.com.pagantis.admin.user.infrastructure.controllers.DTOs;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public final class UserGetDTO
{
    private final String  id;
    private final String  name;
    private final String  surname;
    private final String  email;
}
