package PagaCoin.admin.main.com.pagantis.admin.user.domain;

import PagaCoin.shared.main.com.pagantis.shared.domain.Identifier;

public final class UserId extends Identifier
{
    public UserId(String value)
    {
        super(value);
    }
}
