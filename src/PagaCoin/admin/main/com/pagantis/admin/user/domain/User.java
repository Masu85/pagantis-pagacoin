package PagaCoin.admin.main.com.pagantis.admin.user.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public final class User
{
    private final UserId id;
    private final String name;
    private final String surname;
    private final String email;
    private final Boolean isActive;
}
