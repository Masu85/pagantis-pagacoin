package PagaCoin.admin.main.com.pagantis.admin.user.infrastructure.repositories;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Entity(name = "Users")
@RequiredArgsConstructor
@AllArgsConstructor
public class UserDAO
{
    @Id
    @GeneratedValue(generator = "uuid3")
    @GenericGenerator(name = "uuid3", strategy = "org.hibernate.id.UUIDGenerator")
    private  UUID      user_id;
    private  String    name;
    private  String    surname;
    private  String    email;

    @ColumnDefault("true")
    private  Boolean   isActive;

    @CreationTimestamp
    private  Timestamp creation_date;
}

