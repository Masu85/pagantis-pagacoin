package PagaCoin.admin.main.com.pagantis.admin.user.application;

import PagaCoin.admin.main.com.pagantis.admin.user.domain.User;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserId;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SearchUserByID
{
    private final UserRepository userRepository;

    public Optional<User> searchById(UserId userId)
    {
        return this.userRepository.searchById(userId) ;
    }
}
