package PagaCoin.admin.main.com.pagantis.admin.user.infrastructure.repositories;

import PagaCoin.admin.main.com.pagantis.admin.user.domain.User;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserId;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserRepositoryH2 implements UserRepository
{
    private final UserRepositoryH2Spring userRepositoryH2Spring;

    @Override
    public List<User> searchAll()
    {
        return userRepositoryH2Spring.findAll()
                                     .stream()
                                     .map(userDAO -> new User(new UserId(userDAO.getUser_id().toString()),
                                                              userDAO.getName(),
                                                              userDAO.getSurname(),
                                                              userDAO.getEmail(),
                                                              userDAO.getIsActive()))
                                     .collect(Collectors.toList());
    }

    @Override
    public Optional<User> searchById(UserId userId)
    {

        return userRepositoryH2Spring.findById(UUID.fromString(userId.value()))
                                     .map(userDAO -> new User(new UserId(userDAO.getUser_id().toString()),
                                                              userDAO.getName(),
                                                              userDAO.getSurname(),
                                                              userDAO.getEmail(),
                                                              userDAO.getIsActive()));

    }

    @Override
    public Optional<UserId> createUser(User user)
    {
        return Optional.of(new UserId(userRepositoryH2Spring.save(new UserDAO(null,
                                                                  user.getName(),
                                                                  user.getSurname(),
                                                                  user.getEmail(),
                                                                  true,
                                                                  null))
                                                            .getUser_id().toString()));
    }
}
