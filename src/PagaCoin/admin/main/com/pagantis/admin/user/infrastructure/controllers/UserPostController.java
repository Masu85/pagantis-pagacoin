package PagaCoin.admin.main.com.pagantis.admin.user.infrastructure.controllers;

import PagaCoin.admin.main.com.pagantis.admin.user.application.CreateUser;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.User;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserId;
import PagaCoin.admin.main.com.pagantis.admin.user.infrastructure.controllers.DTOs.UserPostDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
public final class UserPostController
{
    private final CreateUser createUser;

    @PostMapping("/users")
    public final ResponseEntity createNewUser(@RequestBody UserPostDTO userPostDTO)
    {
        Optional<UserId> userId = this.createUser.create(new User(null, userPostDTO.getName(), userPostDTO.getSurname(), userPostDTO.getEmail(), null));

        return userId.isPresent() ? ResponseEntity.ok().body(userId.get().value()) : ResponseEntity.badRequest().body("User creation error") ;
    }

}
