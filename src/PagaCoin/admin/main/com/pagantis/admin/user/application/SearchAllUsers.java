package PagaCoin.admin.main.com.pagantis.admin.user.application;

import PagaCoin.admin.main.com.pagantis.admin.user.domain.User;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public final class SearchAllUsers
{
    private final UserRepository userRepository;

    public List<User> search()
    {
        return this.userRepository.searchAll();
    }
}
