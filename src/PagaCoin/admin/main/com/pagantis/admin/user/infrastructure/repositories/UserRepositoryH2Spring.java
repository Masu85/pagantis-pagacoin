package PagaCoin.admin.main.com.pagantis.admin.user.infrastructure.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepositoryH2Spring extends JpaRepository<UserDAO, UUID>
{
}
