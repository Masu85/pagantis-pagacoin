package PagaCoin.admin.main.com.pagantis.admin.user.application;

import PagaCoin.admin.main.com.pagantis.admin.user.domain.User;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserId;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CreateUser
{
    private final UserRepository userRepository;

    public Optional<UserId> create(User user)
    {
        return this.userRepository.createUser(user);
    }
}
