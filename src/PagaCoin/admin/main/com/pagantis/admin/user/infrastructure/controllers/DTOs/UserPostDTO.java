package PagaCoin.admin.main.com.pagantis.admin.user.infrastructure.controllers.DTOs;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class UserPostDTO
{
    private final String  name;
    private final String  surname;
    private final String  email;
}
