package PagaCoin.admin.main.com.pagantis.admin.user.infrastructure.controllers;

import PagaCoin.admin.main.com.pagantis.admin.user.application.SearchAllUsers;
import PagaCoin.admin.main.com.pagantis.admin.user.application.SearchUserByID;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserId;
import PagaCoin.admin.main.com.pagantis.admin.user.infrastructure.controllers.DTOs.UserGetDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public final class UserGetController
{
    private final SearchAllUsers searchAllUsers;
    private final SearchUserByID searchUserByID;

    @GetMapping("/users/all")
    public ResponseEntity getAllUsers()
    {
        return ResponseEntity.ok().body(searchAllUsers.search()
                                                      .stream()
                                                      .map(user -> new UserGetDTO(user.getId().value(),
                                                                                  user.getName(),
                                                                                  user.getSurname(),
                                                                                  user.getEmail())));
    }

    @GetMapping("/users/{id}")
    public ResponseEntity getUserById(@PathVariable UUID id)
    {
        return ResponseEntity.ok().body(searchUserByID.searchById(new UserId(id.toString()))
                                                      .map(user -> new UserGetDTO(user.getId().value(),
                                                                                  user.getName(),
                                                                                  user.getSurname(),
                                                                                  user.getEmail())));
    }
}
