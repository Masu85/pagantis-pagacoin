package PagaCoin.admin.main.com.pagantis.admin.user.domain;

import java.util.List;
import java.util.Optional;

public interface UserRepository
{
    List<User>       searchAll();
    Optional<User>   searchById(UserId userId);
    Optional<UserId> createUser(User user);
}
