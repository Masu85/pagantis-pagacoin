package PagaCoin.admin.main.com.pagantis.admin.transaction.application;

import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.Transaction;
import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.TransactionId;
import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CreateTransaction
{
    private final TransactionRepository transactionRepository;

    public Optional<TransactionId> create(Transaction transaction)
    {
        return this.transactionRepository.createTransaction(transaction);
    }
}
