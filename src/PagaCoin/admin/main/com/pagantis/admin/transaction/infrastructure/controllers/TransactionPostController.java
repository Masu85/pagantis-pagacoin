package PagaCoin.admin.main.com.pagantis.admin.transaction.infrastructure.controllers;

import PagaCoin.admin.main.com.pagantis.admin.transaction.application.CreateTransaction;
import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.Transaction;
import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.TransactionId;
import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.vo.AmountVO;
import PagaCoin.admin.main.com.pagantis.admin.transaction.infrastructure.controllers.DTOs.TransactionPostDTO;
import PagaCoin.admin.main.com.pagantis.admin.wallet.application.SearchWallet;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletId;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class TransactionPostController
{
    private final CreateTransaction createTransaction;
    private final SearchWallet searchWallet;

    @PostMapping("/transactions")
    public ResponseEntity createNewTransaction(@RequestBody TransactionPostDTO transactionPostDTO)
    {
        //Validate Wallets
        if(!this.searchWallet.serachById(new WalletId(transactionPostDTO.getOriginWallet())).isPresent())
        {
            return ResponseEntity.badRequest().body("Origin wallet not found error");
        }
        if(!this.searchWallet.serachById(new WalletId(transactionPostDTO.getDestinationWallet())).isPresent())
        {
            return ResponseEntity.badRequest().body("Destination wallet not found error");
        }

        Optional<TransactionId> transactionId = createTransaction.create(new Transaction(null,
                                                                                         new WalletId(transactionPostDTO.getOriginWallet()),
                                                                                         new WalletId(transactionPostDTO.getDestinationWallet()),
                                                                                         new AmountVO(transactionPostDTO.getAmount()),
                                                                                         null));

        return transactionId.isPresent() ? ResponseEntity.ok().body(transactionId.get().value()) : ResponseEntity.badRequest().body("Transaction creation error") ;
    }
}
