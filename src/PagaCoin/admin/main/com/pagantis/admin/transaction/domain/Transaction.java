package PagaCoin.admin.main.com.pagantis.admin.transaction.domain;

import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.vo.AmountVO;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletId;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.sql.Timestamp;

@Getter
@RequiredArgsConstructor
public final class Transaction
{
    private final TransactionId id;
    private final WalletId      originWallet;
    private final WalletId      destinationWallet;
    private final AmountVO      amount;
    private final Timestamp     transactionDate;
}
