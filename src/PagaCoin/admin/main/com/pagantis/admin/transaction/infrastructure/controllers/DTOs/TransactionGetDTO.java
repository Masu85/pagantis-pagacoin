package PagaCoin.admin.main.com.pagantis.admin.transaction.infrastructure.controllers.DTOs;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class TransactionGetDTO
{
    private final String     id;
    private final String     originWallet;
    private final String     destinationWallet;
    private final String     amount;
    private final String     transactionDate;
}
