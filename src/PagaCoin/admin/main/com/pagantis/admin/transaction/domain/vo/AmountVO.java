package PagaCoin.admin.main.com.pagantis.admin.transaction.domain.vo;

import lombok.Getter;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Getter
public class AmountVO
{
    private BigDecimal value;

    public AmountVO(BigDecimal amount)
    {
        assureValid(amount);
        this.value = amount.setScale(2, RoundingMode.DOWN);
    }

    public AmountVO(String stringamount)
    {
        try
        {
            BigDecimal amount = new BigDecimal(stringamount);

            assureValid(amount);
            this.value = amount.setScale(2, RoundingMode.DOWN);
        }
        catch(NumberFormatException e)
        {
            throw new RuntimeException("Amount number or format is wrong");
        }
    }

    private void assureValid(BigDecimal amount)
    {
        if(amount == null)
        {
            throw new RuntimeException("Amount cannot be null");
        }

        if(amount.doubleValue() < 0)
        {
            throw new RuntimeException("Amount cannot be negative");
        }
    }
}
