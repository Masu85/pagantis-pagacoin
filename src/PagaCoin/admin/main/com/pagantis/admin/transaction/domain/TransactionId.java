package PagaCoin.admin.main.com.pagantis.admin.transaction.domain;

import PagaCoin.shared.main.com.pagantis.shared.domain.Identifier;

public final class TransactionId extends Identifier
{
    public TransactionId(String value)
    {
        super(value);
    }
}
