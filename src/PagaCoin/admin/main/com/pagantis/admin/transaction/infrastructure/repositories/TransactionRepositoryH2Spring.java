package PagaCoin.admin.main.com.pagantis.admin.transaction.infrastructure.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TransactionRepositoryH2Spring extends JpaRepository<TransactionDAO, UUID>
{
    List<TransactionDAO> findAllByOriginWalletId(UUID walletId);
    List<TransactionDAO> findAllByDestinationWallet(UUID walletId);
    List<TransactionDAO> findAllByOriginWalletIdOrDestinationWallet(UUID originId, UUID destinationId);
}
