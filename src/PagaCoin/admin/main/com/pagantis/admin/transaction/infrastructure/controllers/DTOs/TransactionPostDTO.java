package PagaCoin.admin.main.com.pagantis.admin.transaction.infrastructure.controllers.DTOs;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class TransactionPostDTO
{
    private final String     originWallet;
    private final String     destinationWallet;
    private final String     amount;
}
