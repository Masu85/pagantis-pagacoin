package PagaCoin.admin.main.com.pagantis.admin.transaction.infrastructure.repositories;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Entity(name = "Transactions")
@RequiredArgsConstructor
@AllArgsConstructor
public class TransactionDAO
{
    @Id
    @Column(name = "transaction_id")
    @GeneratedValue(generator = "uuid3")
    @GenericGenerator(name = "uuid3", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID       transactionId;

    @Column(name = "origin_wallet_id")
    private UUID       originWalletId;

    @Column(name = "destination_wallet_id")
    private UUID       destinationWallet;

    private BigDecimal amount;

    @CreationTimestamp
    @Column(name = "transaction_date")
    private Timestamp transactionDate;
}
