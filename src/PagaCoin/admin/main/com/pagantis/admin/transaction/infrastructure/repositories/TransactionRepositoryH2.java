package PagaCoin.admin.main.com.pagantis.admin.transaction.infrastructure.repositories;

import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.Transaction;
import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.TransactionId;
import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.TransactionRepository;
import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.vo.AmountVO;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class TransactionRepositoryH2 implements TransactionRepository
{
    private final TransactionRepositoryH2Spring transactionRepositoryH2Spring;

    @Override
    public List<Transaction> searcAll()
    {
        return transactionRepositoryH2Spring.findAll()
                                            .stream()
                                            .map(transactionDAO -> new Transaction(new TransactionId(transactionDAO.getTransactionId().toString()),
                                                                                   new WalletId(transactionDAO.getOriginWalletId().toString()),
                                                                                   new WalletId(transactionDAO.getDestinationWallet().toString()),
                                                                                   new AmountVO(transactionDAO.getAmount()),
                                                                                   transactionDAO.getTransactionDate()))
                                            .collect(Collectors.toList());
    }

    @Override
    public List<Transaction> searchByWalletId(WalletId walletId)
    {
        return transactionRepositoryH2Spring.findAllByOriginWalletIdOrDestinationWallet(UUID.fromString(walletId.value()), UUID.fromString(walletId.value()))
                                            .stream()
                                            .map(transactionDAO -> new Transaction(new TransactionId(transactionDAO.getTransactionId().toString()),
                                                                                   new WalletId(transactionDAO.getOriginWalletId().toString()),
                                                                                   new WalletId(transactionDAO.getDestinationWallet().toString()),
                                                                                   new AmountVO(transactionDAO.getAmount()),
                                                                                   transactionDAO.getTransactionDate()))
                                            .collect(Collectors.toList());
    }

    @Override
    public Optional<TransactionId> createTransaction(Transaction transaction)
    {
        return Optional.of(new TransactionId((transactionRepositoryH2Spring.save(new TransactionDAO(null,
                                                                                 UUID.fromString(transaction.getOriginWallet().value()),
                                                                                 UUID.fromString(transaction.getDestinationWallet().value()),
                                                                                 transaction.getAmount().getValue(),
                                                                                 null))
                                                                            .getTransactionId().toString())));
    }
}
