package PagaCoin.admin.main.com.pagantis.admin.transaction.application;

import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.Transaction;
import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.TransactionRepository;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SearchTransactionsByWalletId
{
    private final TransactionRepository transactionRepository;

    public List<Transaction> searchByWalletId(WalletId walletId)
    {
        return this.transactionRepository.searchByWalletId(walletId);
    }
}
