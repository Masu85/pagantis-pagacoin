package PagaCoin.admin.main.com.pagantis.admin.transaction.domain;

import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletId;

import java.util.List;
import java.util.Optional;

public interface TransactionRepository
{
    List<Transaction> searcAll();
    List<Transaction> searchByWalletId(WalletId walletId);
    Optional<TransactionId> createTransaction(Transaction transaction);
}
