package PagaCoin.admin.main.com.pagantis.admin.transaction.application;

import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.Transaction;
import PagaCoin.admin.main.com.pagantis.admin.transaction.domain.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SearchAllTransactions
{
    private final TransactionRepository transactionRepository;

    public List<Transaction> search()
    {
        return this.transactionRepository.searcAll();
    }
}
