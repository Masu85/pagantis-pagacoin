package PagaCoin.admin.main.com.pagantis.admin.transaction.infrastructure.controllers;

import PagaCoin.admin.main.com.pagantis.admin.transaction.application.SearchAllTransactions;
import PagaCoin.admin.main.com.pagantis.admin.transaction.application.SearchTransactionsByWalletId;
import PagaCoin.admin.main.com.pagantis.admin.transaction.infrastructure.controllers.DTOs.TransactionGetDTO;
import PagaCoin.admin.main.com.pagantis.admin.wallet.domain.WalletId;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class TransactionGetController
{
    private final SearchAllTransactions searchAllTransactions;
    private final SearchTransactionsByWalletId searchTransactionsByWalletId;

    @GetMapping("/transactions/all")
    public ResponseEntity getTransactions()
    {
        return ResponseEntity.ok(searchAllTransactions.search()
                                                      .stream()
                                                      .map(transaction -> new TransactionGetDTO(transaction.getId().value(),
                                                                                                transaction.getOriginWallet().value(),
                                                                                                transaction.getDestinationWallet().value(),
                                                                                                transaction.getAmount().getValue().toString(),
                                                                                                transaction.getTransactionDate().toString())));
    }

    @GetMapping("/transactions/wallet/{walletId}")
    public ResponseEntity getTransactionsByWalletId(@PathVariable UUID walletId)
    {
        return ResponseEntity.ok(searchTransactionsByWalletId.searchByWalletId(new WalletId(walletId.toString()))
                                                             .stream()
                                                             .map(transaction -> new TransactionGetDTO(transaction.getId().value(),
                                                                     transaction.getOriginWallet().value(),
                                                                     transaction.getDestinationWallet().value(),
                                                                     transaction.getAmount().getValue().toString(),
                                                                     transaction.getTransactionDate().toString())));

    }

}
