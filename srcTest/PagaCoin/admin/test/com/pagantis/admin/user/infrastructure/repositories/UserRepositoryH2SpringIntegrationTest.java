package PagaCoin.admin.test.com.pagantis.admin.user.infrastructure.repositories;

import PagaCoin.admin.main.com.pagantis.admin.user.infrastructure.repositories.UserDAO;
import PagaCoin.admin.main.com.pagantis.admin.user.infrastructure.repositories.UserRepositoryH2Spring;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringRunner.class)
public class UserRepositoryH2SpringIntegrationTest
{
    @Autowired
    private TestEntityManager       entityManager;

    @Autowired
    private UserRepositoryH2Spring  userRepositoryH2Spring;

    @Test
    public void whenFindById_thenReturnOptionalWithUserDAO()
    {
        UserDAO userTest = new UserDAO(null,
                "nameTest",
                "surnameTest",
                "email@test.com",
                true,
                null);

        entityManager.persist(userTest);
        entityManager.flush();

        Optional<UserDAO> userFounded = userRepositoryH2Spring.findById(userTest.getUser_id());

        assertThat(userFounded.isPresent());
        assertThat(userFounded.get() instanceof UserDAO);
        assertThat(userFounded.get().getUser_id()).isEqualTo(userTest.getUser_id());
    }

    @Test
    public void whenNotFondById_thenReturnEmptyOptional()
    {
        Optional<UserDAO> userFounded = userRepositoryH2Spring.findById(UUID.randomUUID());
        assertThat(!userFounded.isPresent());
    }

    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void whenNoId_thenThrowException()
    {
        Optional<UserDAO> userFounded = userRepositoryH2Spring.findById(null);
    }
}
