package PagaCoin.admin.test.com.pagantis.admin.user.application;

import PagaCoin.admin.main.com.pagantis.admin.user.application.SearchUserByID;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.User;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserId;
import PagaCoin.admin.main.com.pagantis.admin.user.domain.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class SearchUserByIDIntegrationTest
{
    private UUID   testUUID;
    private UserId testUserId;
    private SearchUserByID searchUserByID;

    @MockBean
    private UserRepository userRepository;

    @Before
    public void setUp()
    {
        this.testUUID       = UUID.randomUUID();
        this.testUserId     = new UserId(testUUID.toString());
        this.searchUserByID = new SearchUserByID(userRepository);

        User userTest = new User(testUserId,
                                "nameTest",
                                "surnameTest",
                                "email@test.com",
                                true);

        Mockito.when(userRepository.searchById(testUserId))
               .thenReturn(Optional.of(userTest));
    }

    @Test
    public void whenValidUserID_thenUserShouldBeFound()
    {
        UserId userId = testUserId;
        Optional<User> userFound = this.searchUserByID.searchById(userId);

        assertThat(userFound.isPresent());
        assertThat(userFound.get().getId().value()).isEqualTo(testUUID.toString());
    }

}
