#Pagantis - PagaCoin Admin application

To deploy from project

####BackEnd 

    - Execute: mvnw clean install

    - Exeecute: mvnw spring-boot:run
    
    - Or you can generate .jar executing: mvnw package

####FrontEnd  

    - Navigate to: ./apps/pagaCoin

    - Execute: npm install
    
    - Execute: ng serve


To deploy on Docker contanier, execute from application root folder 

####BackEnd 

    Build Application and Generate Jar: 
        mvnw package
        
    Build Docker Image: 
        docker build -t pagantis/pagacoin .
        
    Run Docker Image:
        docker run -p 8080:8080 pagantis/pagacoin

####FrontEnd     

    Build Docker Image:
        docker build -t pagacoin/admin ./apps/pagaCoin/
        
    Run Docker Image:
        docker run -p 80:80 --name angular pagacoin/admin
        

#Links

- Backend: http://localhost:8080/

- Frontend: 
    - Deployed from project: http://localhost:4200/
    - Deployed in Docker:    http://localhost/

- Swagger: http://localhost:8080/swagger-ui.html
